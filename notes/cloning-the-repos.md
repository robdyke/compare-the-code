# cloning the repos

### Clone stats

Repo | Objects | Size
---- | ------- | ----
opusVL/Open-eObs-Modules | 13872 | 12.52 MiB
openeobs/openeobs | 33344 | 15.37 MiB
openeobs/nhclinical | 12918 | 8.58 MiB
openeobs/openeobs-client-modules | 1303 | 1.05 MiB


### Output

```sh
robd@rd-nuc:~/Work/code-compare/opusvl$ git clone git@gitlab.com:openeobs/opusVL/Open-eObs-Modules.git
Cloning into 'Open-eObs-Modules'...
remote: Enumerating objects: 13872, done.
remote: Counting objects: 100% (13872/13872), done.
remote: Compressing objects: 100% (5440/5440), done.
remote: Total 13872 (delta 7723), reused 13872 (delta 7723)
Receiving objects: 100% (13872/13872), 12.52 MiB | 571.00 KiB/s, done.
Resolving deltas: 100% (7723/7723), done.
```

```sh
robd@rd-nuc:~/Work/code-compare/openeobs$ git clone git@gitlab.com:openeobs/openeobs.git
Cloning into 'openeobs'...
remote: Enumerating objects: 33344, done.
remote: Counting objects: 100% (33344/33344), done.
remote: Compressing objects: 100% (10583/10583), done.
remote: Total 33344 (delta 21265), reused 33334 (delta 21260)
Receiving objects: 100% (33344/33344), 15.37 MiB | 514.00 KiB/s, done.
Resolving deltas: 100% (21265/21265), done.
```

```sh
robd@rd-nuc:~/Work/code-compare/compare-the-code$ git clone git@gitlab.com:openeobs/nhclinical.git
Cloning into 'nhclinical'...
remote: Enumerating objects: 12918, done.
remote: Counting objects: 100% (12918/12918), done.
remote: Compressing objects: 100% (4454/4454), done.
remote: Total 12918 (delta 7880), reused 12917 (delta 7879)
Receiving objects: 100% (12918/12918), 8.58 MiB | 576.00 KiB/s, done.
Resolving deltas: 100% (7880/7880), done.
```

```sh
robd@rd-nuc:~/Work/code-compare/compare-the-code$ git clone git@gitlab.com:openeobs/openeobs-client-modules.git
Cloning into 'openeobs-client-modules'...
remote: Enumerating objects: 1303, done.
remote: Total 1303 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (1303/1303), 1.05 MiB | 552.00 KiB/s, done.
Resolving deltas: 100% (600/600), done.
```
