# References

Oh Yeah? Fork You! https://blog.codinghorror.com/oh-yeah-fork-you/
> Because open source code is freely distributable, anyone can take that code and create their own unique mutant mashup version of it any time they feel like it. Whether anyone else in the world will care about their crazy new version of the code is not at all clear, but that's not the point. If someone wants it bad enough, they can create it -- or pay someone else to create it for them. This is known as "forking". It's the very embodiment of freedom zero, and it's an essential part of every open source license.

Forking protocol - https://jamesdixon.wordpress.com/forking-protocol-why-when-and-how-to-fork-an-open-source-project/
> Why, When, and How to Fork an Open Source Project

Getting Legit with Git (and GitHub): Cloning and Forking https://thenewstack.io/getting-legit-with-git-and-github-cloning-and-forking/
> Today’s lesson is going to be about forking and cloning repositories, which builds upon the working with Git’s branch feature. Please take a quick spin through that if you need a quick refresher, because the ability to create branches and merge them back into master is (1) an essential skill and (2) utterly necessary to take this next step in effectively working with version control on projects, whether solo or collaborating with a team.


Open Source Guides https://opensource.guide/
> Open source software is made by people just like you. Learn how to launch and grow your project.

The Principles of Community-Oriented GPL Enforcement https://www.fsf.org/licensing/enforcement-principles
> Following the GPL's terms is easy — it gets more complicated only when products distributed with GPL'd software also include software distributed under terms that restrict users. Even in these situations, many companies comply properly, but some companies also try to bend or even break the GPL's rules to their perceived advantage.

How to deal with copyright infringement on GitHub https://opensource.stackexchange.com/questions/5817/how-to-deal-with-copyright-infringement-on-github

```
Hello {Downstream Author},

I noticed that you're using code from my software project {Foo} in your project {Bar}. I'm really glad that you found it useful. I put it under the Apache License exactly because I wanted projects like yours to use it freely.

I did notice, though, that you left out a few important pieces that the license requires. These are important but fortunately they're really easy to fix:

Each file that uses code from {Foo} should preserve the original license header -- this is important so that other people down the line also know they have the right to modify and/or redistribute my software, just like you did. Your project should also include a copy of the Apache 2 license with the full license terms in your project, in case recipients have legal questions about their rights. (http://www.apache.org/licenses/LICENSE-2.0.txt) If your own code is licensed differently from Apache 2, you can state exactly which functions are licensed which way in each file.

Each file that uses my code should include my copyright notice, so the people know who the original author was. If a file includes work from both you and from me, it should have copyright notices for each of us.

You can see an example of what the header should look like at {https://myrepo/myproject/somefile.c}

If you want to take care of it, that would be great; otherwise, I can send you a pull request with the header fixes.
```
