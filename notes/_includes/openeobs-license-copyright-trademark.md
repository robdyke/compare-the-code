## open-eObs License, Copyright & Trademark

### License

Released under the GNU Affero General Public License, Version 3, 19 November 2007.

### Copyright

Copyright (C) Neova Health LLP (2014-2019)

Copyright (C) BJSS Limited (2017-2019)

### Trademark

The open-eObs identity is a protected trademark, registered to Open Health Hub CIC. Registration UK00003101467.
