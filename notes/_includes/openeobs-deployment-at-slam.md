## open-eObs as deployed at SLaM

Module | SLaM version | Latest release
------ | ------------ | ---------
openeobs | v1.11.0 | v1.12.0
nhclinical | v1.11.0 | v1.12.0
odoo | v8.0 + v1.11.0 | v8.0-1.12.0
adt-connector | v2.10-0.2.4 | v2.10-0.2.5

### SLaM versions in production environment
- openeobs [v1.11.0](https://github.com/openeobs/openeobs/releases/tag/1.11.0)
  - opusvl's open-eObs-modules [@f3410aa](https://github.com/OpusVL/Open-eObs-Modules/commit/f3410aafa4b9165c3037da4a16b7cce43db2389e)
- nhclinical [v1.11.0](https://github.com/openeobs/openeobs/releases/tag/1.11.0)
  - opusvl's open-eObs-modules [@f3410aa](https://github.com/OpusVL/Open-eObs-Modules/commit/f3410aafa4b9165c3037da4a16b7cce43db2389e)
- adt-connector [v2.10-0.2.4](https://github.com/NeovaHealth/adt-connector/releases/tag/2.10-0.2.4)
