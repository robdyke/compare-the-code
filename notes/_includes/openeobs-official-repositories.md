## open-eObs Official Repositories

The official source for open-eObs is https://github.com/openeobs

### Modules

Name | Description
---- | -------
[openeobs](https://github.com/openeobs/openeobs) | Odoo modules that add e-observations and ward management tools for Acute and Mental Health hospitals.
[openeobs-client-modules](https://github.com/openeobs/openeobs-client-modules) | Modules that customise Open-eObs for clients.
[nhclinical](https://github.com/openeobs/nhclinical) | Odoo modules that add clinical models and healthcare functionality to Odoo.
[odoo](https://github.com/openeobs/odoo) | Odoo v8.0

### Docker
Name | Description
---- | -------
[openeobs-odoo-docker-base](https://github.com/openeobs/openeobs-odoo-docker-base) | A Debian based Docker image with the dependencies for Odoo v8 installed.
[openeobs-odoo-docker](https://github.com/openeobs/openeobs-odoo-docker) | A vanilla Odoo Docker image ready for open-eObs, to be used in a CI pipeline.
[openeobs-docker](https://github.com/openeobs/openeobs-docker) | open-eobs as a Docker container with configurable nhclinical, openeobs openeobs-client-modules installation.

### Quality Assurance and Automation
Name | Description
---- | -------
[openeobs-automation](https://github.com/openeobs/openeobs-automation) | Easy-to-read executable specifications that provide E2E testing of open-eObs.
[openeobs-selenium](https://github.com/openeobs/openeobs-selenium) | Selenium selectors and Page Object Models for open-eObs.
[maintainer-quality-tools](https://github.com/openeobs/maintainer-quality-tools) | QA tools for Odoo maintainers
