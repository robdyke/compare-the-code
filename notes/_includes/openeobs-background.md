## open-eObs Background

open-eObs was released in 2014. These early releases were developed by Neova Health with clinical input from Luton and Dunstable NHS Foundation Trust, Kings College Hospital and Basildon and Thurrock University Hospitals. NHS England funded some early work through technology fund programme awards.

In 2016 open-eObs was adopted by BJSS. An edition of the application for mental health organisations was produced with clinical governance from South London and the Maudsley NHS Foundation Trust.

Today open-eObs is clinically led by Adrian Burke, consultant psychiatrist and CCIO at Cheshire and Wirral Partnership NHS Foundation Trust.

Adrian is Chair of the Apperta Foundation open-eObs committee, sits on the Royal College of Psychiatrists' Informatics Committee and is a member of the Digital Health CCIO network.

The project is now open-eObs is hosted by Open Health Hub, a Not For Profit founded as a Community Interest Company (CIC) in 2012.
