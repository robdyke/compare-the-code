## OpusVL's open-eobs-modules

### TL/DR
OpusVL's repository Open-eObs-Modules
- Does not track any of the official open-eObs repositories
- Is based on a mirror of `openeobs/nhclinical`
- Amalgamates code from the `openeobs/openeobs` and `openeobs/openeobs-client-modules` repositories

General observations on the default branch
- The LICENSE has been removed
- The COPYRIGHT has been removed
- The README has been removed
- CONTRIBUTING guidance and .github templates for issues and pull requests are not present / have been removed.
- CI testing configuration file (.travis.yml) is not present / has been removed.
- Code coverage and quality review configuration file (sonar-project.properties) is not present / has been removed.
- Detailed module documentation is not found / has been removed.
- ReadTheDocs configuration is not present / has been removed.

### Summary

Since being engaged by SLaM, OpusVL have added new features and resolved bugs in the version of open-eObs deployed at the Trust. These contributions are available from the OpusVL `Open-eobs-modules` repository published at https://github.com/OpusVL/Open-eObs-Modules.

As good open-source citizens, the Trust and OpusVL should make these changes available to the wider open-eObs community.

However OpusVL do not track the official repositories of open-eObs, opting instead to amalgamate the three upstream repositories into a single repository. We need to identify the source(s) used to create this single repo and compare each source with the official upstream repositories before the Trust can share fixes and features with the wider open-eObs community.

In July 2018 the Open eObs Subcommittee of the Apperta Foundation CIC held a Strategy Meeting chaired by Adrian Burke (Cheshire and Wirral Partnership NHS Foundation Trust) and attended by Peter Coates (NHS Digital), Eckhard Schwarzat (Neova Health), Haddy Quist (South London and Maudsley NHS Foundation Trust), Denise Kyte (OpusVL) and others. Discussed at this meeting was the convergence of the SLaM deployment with the open-eObs repositories. The Apperta Foundation recognised the value to the open-eObs community of incorporating SLaMs development activities with the community repositories and may allocate funding towards the costs of integrating the features and fixes in the OpusVL repo with the official sources.

<div style="page-break-after: always;"></div>

### Investigation
#### Identify the origin of OpusVL/open-eobs-modules
The OpusVL repo `open-eobs-modules` was created as a mirror of `openeobs/nhclinical` as these repositories share git history.

![Screenshot of initial commit to opusvl open-eobs-modules master](opusvl-openeobs-modules/repo-insights/Network-James-Curtis-initial-commit.png)
*Screenshot of `OpusVL/Open-eObs-Modules` master branch*

The [first commit](https://github.com/OpusVL/Open-eObs-Modules/commit/224f08b960bbcee9c0206bc438c6b444b4dae0ad) to `Open-eObs-Modules` by OpusVL was from James Curtis on 16 Nov 2017.

    Commit:224f08b960bbcee9c0206bc438c6b444b4dae0ad [224f08b]
    Author:James Curtis <james.curtis@opusvl.com>
    Date:16 Nov 2017 09:44:58

The [previous commit](https://github.com/OpusVL/Open-eObs-Modules/commit/edab0e93306f39e7aea57df3a46e240a78c81a38) to `Open-eObs-Modules` in the git log is from Colin Wren for BJSS on the 24 October 2017. This commit ([edab0e9](https://github.com/openeobs/nhclinical/commit/edab0e93306f39e7aea57df3a46e240a78c81a38)) is also found in `openeobs/nhclinical`.

    Commit:edab0e93306f39e7aea57df3a46e240a78c81a38 [edab0e9]
    Author:Colin Wren <colin@gimpneek.com>
    Date:24 Oct 2017 15:40:07

<div style="page-break-after: always;"></div>

#### Identify the code included in OpusVL/Open-eObs-Modules.
The OpusVL repo `open-eobs-modules` incorporates code from the open-eObs repos `openeobs` & `openeobs-client-modules`.

![Screenshot of commits to opusvl open-eobs-modules prod](opusvl-openeobs-modules/commits/PROD-Commits-OpusVL-Open-eObs-Modules.png)
*Screenshot of commits to OpusVL/Open-eObs-Modules*

This [commit](https://github.com/OpusVL/Open-eObs-Modules/commit/e02d930e45f526b7189d79b3e9c43d754f89fcaf) by James Curtis for OpusVL on the 19 Dec 2017 shows 104 changed files with 20,937 additions. Added by this commit is code from `nh_eobs_slam`, found in `openeobs/openeobs-client-modules`

    Commit:e02d930e45f526b7189d79b3e9c43d754f89fcaf [e02d930]
    Author:James Curtis <james.curtis@opusvl.com>
    Date:19 Dec 2017
    [ADD] SLaM modules

This [commit](https://github.com/OpusVL/Open-eObs-Modules/commit/5b24c14d2866c1ec5cb94da9fa805fd23ab36e58) by James Curtis for OpusVL on the 14 Feb 2018 shows 1,981 changed files with 433,033 additions and 2,242 deletions. Added by this commit is code found in `openeobs/openeobs` & `openeobs/openeobs-client-modules`.

    Commit:5b24c14d2866c1ec5cb94da9fa805fd23ab36e58 [5b24c14]
    Author:James Curtis <james.curtis@opusvl.com>
    Date:14 Feb 2018 15:06:05
    [ADD] Production modules

I suspect that the source for this commit is a direct copy from the production server at SLaM. This is because this commit adds Python source files compiled into bytecode in the form of .pyc files. These files are generated automatically and on the fly on every machine where the code runs so usually excluded from the code repository. This is because if a .pyc file exists for a module, it's code can be imported even after deleting the module source file. This may lead to strange bugs in the case where a user changes a module and forgets to delete the .pyc file when checking in the changes, as now collaborators or deployments from the repository have some code that still imports from the .pyc without reflecting the changes. See [Python newbies often make the mistake of committing .pyc files in git repositories](https://coderwall.com/p/wrxwog/why-not-to-commit-pyc-files-into-git-and-how-to-fix-if-you-already-did)

<div style="page-break-after: always;"></div>

#### Comparing repository clone sizes
By comparing the size and number of objects in each of the official repositories with the OpusVL amalgamated repository with we can assess the extent of the divergence from the upstream code-base.

Repo | Objects | Size
---- | ------- | ----
openeobs/openeobs | 33344 | 15.37 MiB
openeobs/nhclinical | 12918 | 8.58 MiB
openeobs/openeobs-client-modules | 1303 | 1.05 MiB
Sub-total of openeobs repos | 47565 | 25 MiB
opusVL/Open-eObs-Modules | 13872 | 12.52 MiB

##### Clone summary

```sh
robd@rd-nuc:~/Work/code-compare/opusvl$ git clone git@gitlab.com:openeobs/opusVL/Open-eObs-Modules.git
Cloning into 'Open-eObs-Modules'...
Receiving objects: 100% (13872/13872), 12.52 MiB | 571.00 KiB/s, done.
```

```sh
robd@rd-nuc:~/Work/code-compare/openeobs$ git clone git@gitlab.com:openeobs/openeobs.git
Cloning into 'openeobs'...
Receiving objects: 100% (33344/33344), 15.37 MiB | 514.00 KiB/s, done.
```

```sh
robd@rd-nuc:~/Work/code-compare/compare-the-code$ git clone git@gitlab.com:openeobs/nhclinical.git
Cloning into 'nhclinical'...
Receiving objects: 100% (12918/12918), 8.58 MiB | 576.00 KiB/s, done.
```

```sh
robd@rd-nuc:~/Work/code-compare/compare-the-code$ git clone git@gitlab.com:openeobs/openeobs-client-modules.git
Cloning into 'openeobs-client-modules'...
Receiving objects: 100% (1303/1303), 1.05 MiB | 552.00 KiB/s, done.
```
