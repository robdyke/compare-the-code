# Review

<!-- MDTOC maxdepth:3 firsth1:2 numbering:0 flatten:0 bullets:1 updateOnSave:1 -->

- [Summary](#summary)
   - [TL/DR](#tldr)
   - [Quick Wins](#quick-wins)
   - [Direction of Travel](#direction-of-travel)
- [open-eObs](#open-eobs)
- [SLaM variant](#slam-variant)
- [OpusVL](#opusvl)
   - [Repository concerns](#repository-concerns)
   - [Test / QA removed](#test-qa-removed)
   - [GNU GPL problems](#gnu-gpl-problems)
- [Technical Debt](#technical-debt)
   - [Blocked deployments](#blocked-deployments)
   - [Factors contributing to technical debt.](#factors-contributing-to-technical-debt)
- [Delivery Deficit](#delivery-deficit)
- [Docker](#docker)
- [Testing and Quality Assurance](#testing-and-quality-assurance)
- [Appendices](#appendices)
   - [open-eObs Official Repositories](#open-eobs-official-repositories)
   - [About open-eObs](#about-open-eobs)
      - [License](#license)
      - [Copyright](#copyright)
      - [Trademark](#trademark)
   - [OpusVL's open-eobs-modules repository](#opusvls-open-eobs-modules-repository)
      - [Summary](#summary)
      - [Identify the origin of OpusVL/open-eobs-modules](#identify-the-origin-of-opusvlopen-eobs-modules)
      - [Identify the code included in OpusVL/Open-eObs-Modules.](#identify-the-code-included-in-opusvlopen-eobs-modules)
      - [Comparing repository clone sizes](#comparing-repository-clone-sizes)

<!-- /MDTOC -->

<div class="page-break" />

## Summary

### TL/DR
-   SLaM run a custom variant of open-eObs
-   SLaM are missing out on new features available in open-eObs
-   SLaM are unable to contribute to open-eObs

### Quick Wins
-   Unblock deployments
-   Pay off Technical Debt

### Direction of Travel
-   Make a path to the official code base
-   Address factors that incur debt
-   Focus on quality with clear specs / tests
-   Use automation tooling



<div class="page-break" />

## open-eObs

The current community release of open-eObs is v1.12.0

<table>
  <tr>
    <th width="25%"></th>
    <th width="auto">Release notes for v1.12.0</th>
  </tr>
  <tr>
    <td>open-eObs</td>
    <td>New Modules
			<ul>
		    <li>Shift Allocation and Management module
		    <li>User module for Ward Manager
		    <li>Therapeutic Observations module
				<li>Bed Management module
	    </ul>
			Updated Modules
			<ul>
				<li>PDF report / NEWS chart
				<li>Food and Fluid module
		    <li>CWP and SLaM default settings
			</ul>
	    Quality Assurance
			<ul>
				<li>Improved unit test coverage
		    <li>Complete module documentation
	    </ul>
			Performance
			<ul>
				<li>Further odoo fixes
		    <li>Performance improvements to SQL
			<ul>
    </td>
  </tr>
</table>

A complete repository list can be found in Appendix 1 - open-eObs Official Repositories.

## SLaM variant

SLaM are running a custom variant of open-eObs

<table>
<tr>
<th width="25%"></th>
<th width="auto">Version</th>
</tr>
<tr>
<td>OpusVL's<br/>open-eObs-modules</td>
<td>
<ul>
<li>Unknown version; tracking default branch <a href="https://github.com/OpusVL/Open-eObs-Modules/commit/f3410aafa4b9165c3037da4a16b7cce43db2389e">@f3410aa</a>
<li>Based on nhclinical v1.11.0
<li>Contains unknown version of openeobs and openeobs-client-modules
<li>Contains opusVL patches and custom modules
</ul>
</td>
</tr>
</table>

<div class="page-break" />

## OpusVL

OpusVL's open-eobs-modules repository is the source for the SLaM varient of open-eObs.

### Repository concerns
-   Does not track any of the official open-eObs repositories
-   Is based on a mirror of `openeobs/nhclinical`
-   Amalgamates code from the `openeobs/openeobs` and `openeobs/openeobs-client-modules` repositories

### Test / QA removed
-   CI testing configuration file (.travis.yml) is not present / has been removed.
-   Code coverage and quality review configuration file (sonar-project.properties) is not present / has been removed.
-   Detailed module documentation is not found / has been removed.
-   ReadTheDocs configuration is not present / has been removed.

### GNU GPL problems
-   The LICENSE has been removed
-   The COPYRIGHT has been removed
-   The README has been removed
-   CONTRIBUTING guidance and .github templates for issues and pull requests are not present / have been removed.

<sub>Based on the default branch of `open-eobs-modules`.</sub>


Further details on my review of OpusVL's open-eobs-modules can be found in Appendix 4 'OpusVL's open-eobs-modules repository'.

<div class="page-break" />

## Technical Debt

### Blocked deployments

SLaM feature items
- Multiple Case-Load feature
- PDF to CDR feature

SLaM server environment
- PROD virtual machines not using optium settings
- HTTPS not in use

### Factors contributing to technical debt.

SLaM Clinical System Team
- Lack of UAT tooling

OpusVL
- Alignment with open-eObs repositories
- Weak CI

<div class="page-break" />

## Delivery Deficit

Project throughput is low, with few items delivered/resolved each week/month. Project cycle-time is high, with items taking a lot of time to complete. These are the areas of workflow that need improvement.

SLaM open-eObs project
- Project item tracking
- Business Driven Development
- Test specifications for UAT
- Quality criteria for Deployment

OpusVL
- Continuous Integration testing
- Continuous Delivery to UAT for SLaM review

<div class="page-break" />


## Docker

open-eObs Docker containers are published on Docker Hub https://hub.docker.com/u/openeobs

<table>
  <tr>
    <th width="25%">Git repo</th>
    <th width="auto" align=left>Docker service</th>
  </tr>
	<tr>
		<td><a href="https://github.com/openeobs/openeobs-docker">openeobs-docker</a></td>
		<td>open-eobs as a Docker container with configurable nhclinical, openeobs, and openeobs-client-modules installation.</td>
	</tr>
  <tr>
    <td><a href="https://github.com/openeobs/openeobs-odoo-docker">openeobs-odoo-docker</a></td>
    <td>A vanilla Odoo Docker image ready for open-eObs to be used in a CI pipeline.</td>
  </tr>
	<tr>
		<td><a href="https://github.com/openeobs/openeobs-odoo-docker-base">openeobs-odoo-docker-base<a/></td>
		<td>A Debian based Docker image with the dependencies for Odoo v8 installed.</td>
	</tr>
</table>

<div class="page-break" />

## Testing and Quality Assurance

There are several open-eObs repositories focused on Testing and Quality Assurance.

open-eObs is tested at the following levels:

1. Business rule level. This is the conceptual layer. It is expressed in feature files that describe the business rules in an abstract manner.
1. User workflow level. This is the logical layer. It is expressed in Python modules in the steps package. They should consist of well abstracted methods that describe the logical actions necessary to achieve a user goal.
1. Technical activity level. This is the physical layer. It describes the physical actions that must be taken by the user to perform a workflow. It is expressed in Python modules in the liveobs_ui package.

<table>
<tr>
<th width="25%">Name</th>
<th width="auto" align=left>Description</th>
</tr>
<tr>
<td><a href="https://github.com/openeobs/openeobs-automation">openeobs-automation</a></td>
<td>Easy-to-read executable specifications that provide E2E testing of open-eObs.</td>
</tr>
<tr>
<td><a href="https://github.com/openeobs/openeobs-selenium">openeobs-selenium</a></td>
<td>Selenium selectors and Page Object Models for open-eObs.</td>
</tr>
<tr>
<td><a href="https://github.com/openeobs/maintainer-quality-tools">maintainer-quality-tools</a></td>
<td>QA tools for Odoo maintainers</td>
</tr>
</table>


<div class="page-break" />

## Appendices

<div class="page-break" />

### open-eObs Official Repositories

The official source for open-eObs is <https://github.com/openeobs>

<table>
  <tr>
    <th width="25%">Name</th>
    <th width="auto" align=left>Description</th>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/openeobs">openeobs</a></td>
    <td>Odoo modules that add e-observations and ward management tools for Acute and Mental Health hospitals.</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/openeobs-client-modules">openeobs-client-modules</a></td>
    <td>Modules that customise Open-eObs for clients.</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/nhclinical">nhclinical</a></td>
    <td>Odoo modules that add clinical models and healthcare functionality to Odoo.</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/odoo">odoo</a></td>
    <td>Odoo v8.0</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/openeobs-odoo-docker-base">openeobs-odoo-docker-base<a/></td>
    <td>A Debian based Docker image with the dependencies for Odoo v8 installed.</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/openeobs-odoo-docker">openeobs-odoo-docker</a></td>
    <td>A vanilla Odoo Docker image ready for open-eObs, to be used in a CI pipeline.</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/openeobs-docker">openeobs-docker</a></td>
    <td>open-eobs as a Docker container with configurable nhclinical, openeobs openeobs-client-modules installation.</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/openeobs-automation">openeobs-automation</a></td>
    <td>Easy-to-read executable specifications that provide E2E testing of open-eObs.</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/openeobs-selenium">openeobs-selenium</a></td>
    <td>Selenium selectors and Page Object Models for open-eObs.</td>
  </tr>
  <tr>
    <td><a href="https://github.com/openeobs/maintainer-quality-tools">maintainer-quality-tools</a></td>
    <td>QA tools for Odoo maintainers</td>
  </tr>
</table>

<div class="page-break" />

### About open-eObs

open-eObs was released in 2014. These early releases were developed by Neova Health with clinical input from Luton and Dunstable NHS Foundation Trust, Kings College Hospital and Basildon and Thurrock University Hospitals. NHS England funded some early work through technology fund programme awards.

In 2016 open-eObs was adopted by BJSS. An edition of the application for mental health organisations was produced with clinical governance from South London and the Maudsley NHS Foundation Trust.

Today open-eObs is clinically led by Adrian Burke, consultant psychiatrist and CCIO at Cheshire and Wirral Partnership NHS Foundation Trust.

Adrian is Chair of the Apperta Foundation open-eObs committee, sits on the Royal College of Psychiatrists' Informatics Committee and is a member of the Digital Health CCIO network.

The project is now hosted by Open Health Hub, a Not For Profit Community Interest Company (CIC) founded in 2012.

#### License

Released under the GNU Affero General Public License, Version 3, 19 November 2007.

#### Copyright

Copyright (C) Neova Health LLP (2014-2019)

Copyright (C) BJSS Limited (2017-2019)

#### Trademark

The open-eObs identity is a protected trademark, registered to Open Health Hub CIC. Registration UK00003101467.

<div class="page-break" />

### OpusVL's open-eobs-modules repository

#### Summary

Since being engaged by SLaM, OpusVL have added new features and resolved bugs in the version of open-eObs deployed at the Trust. These contributions are available from the OpusVL `Open-eobs-modules` repository published at <https://github.com/OpusVL/Open-eObs-Modules>.

As good open-source citizens, the Trust and OpusVL should make these changes available to the wider open-eObs community.

However OpusVL do not track the official repositories of open-eObs, opting instead to amalgamate the three upstream repositories into a single repository. We need to identify the source(s) used to create this single repo and compare each source with the official upstream repositories before the Trust can share fixes and features with the wider open-eObs community.

In July 2018 the Open eObs Subcommittee of the Apperta Foundation CIC held a Strategy Meeting chaired by Adrian Burke (Cheshire and Wirral Partnership NHS Foundation Trust) and attended by Peter Coates (NHS Digital), Eckhard Schwarzat (Neova Health), Haddy Quist (South London and Maudsley NHS Foundation Trust), Denise Kyte (OpusVL) and others. Discussed at this meeting was the convergence of the SLaM deployment with the open-eObs repositories.

The Apperta Foundation recognise the value to the open-eObs community of incorporating SLaMs development activities with the community repositories and may allocate funding towards the costs of integrating the features and fixes in the OpusVL repo with the official sources.

<div class="page-break" />

#### Identify the origin of OpusVL/open-eobs-modules

The OpusVL repo `open-eobs-modules` was created as a mirror of `openeobs/nhclinical` as these repositories share git history.

![Screenshot of initial commit to opusvl open-eobs-modules master](opusvl-openeobs-modules/repo-insights/Network-James-Curtis-initial-commit.png)
_Screenshot of `OpusVL/Open-eObs-Modules` master branch_

The [first commit](https://github.com/OpusVL/Open-eObs-Modules/commit/224f08b960bbcee9c0206bc438c6b444b4dae0ad) to `Open-eObs-Modules` by OpusVL was from James Curtis on 16 Nov 2017.

    Commit:224f08b960bbcee9c0206bc438c6b444b4dae0ad [224f08b]
    Author:James Curtis <james.curtis@opusvl.com>
    Date:16 Nov 2017 09:44:58

The [previous commit](https://github.com/OpusVL/Open-eObs-Modules/commit/edab0e93306f39e7aea57df3a46e240a78c81a38) to `Open-eObs-Modules` in the git log is from Colin Wren for BJSS on the 24 October 2017. This commit ([edab0e9](https://github.com/openeobs/nhclinical/commit/edab0e93306f39e7aea57df3a46e240a78c81a38)) is also found in `openeobs/nhclinical`.

    Commit:edab0e93306f39e7aea57df3a46e240a78c81a38 [edab0e9]
    Author:Colin Wren <colin@gimpneek.com>
    Date:24 Oct 2017 15:40:07

<div class="page-break" />

#### Identify the code included in OpusVL/Open-eObs-Modules.

The OpusVL repo `open-eobs-modules` incorporates code from the open-eObs repos `openeobs` & `openeobs-client-modules`.

![Screenshot of commits to opusvl open-eobs-modules prod](opusvl-openeobs-modules/commits/PROD-Commits-OpusVL-Open-eObs-Modules.png)
_Screenshot of commits to OpusVL/Open-eObs-Modules_

This [commit](https://github.com/OpusVL/Open-eObs-Modules/commit/e02d930e45f526b7189d79b3e9c43d754f89fcaf) by James Curtis for OpusVL on the 19 Dec 2017 shows 104 changed files with 20,937 additions. Added by this commit is code from `nh_eobs_slam`, found in `openeobs/openeobs-client-modules`

    Commit:e02d930e45f526b7189d79b3e9c43d754f89fcaf [e02d930]
    Author:James Curtis <james.curtis@opusvl.com>
    Date:19 Dec 2017
    [ADD] SLaM modules

This [commit](https://github.com/OpusVL/Open-eObs-Modules/commit/5b24c14d2866c1ec5cb94da9fa805fd23ab36e58) by James Curtis for OpusVL on the 14 Feb 2018 shows 1,981 changed files with 433,033 additions and 2,242 deletions. Added by this commit is code found in `openeobs/openeobs` & `openeobs/openeobs-client-modules`.

    Commit:5b24c14d2866c1ec5cb94da9fa805fd23ab36e58 [5b24c14]
    Author:James Curtis <james.curtis@opusvl.com>
    Date:14 Feb 2018 15:06:05
    [ADD] Production modules

I suspect that the source for this commit is a direct copy from the production server at SLaM. This is because this commit adds Python source files compiled into bytecode in the form of .pyc files. These files are generated automatically and on the fly on every machine where the code runs so usually excluded from the code repository. This is because if a .pyc file exists for a module, it's code can be imported even after deleting the module source file. This may lead to strange bugs in the case where a user changes a module and forgets to delete the .pyc file when checking in the changes, as now collaborators or deployments from the repository have some code that still imports from the .pyc without reflecting the changes. See [Python newbies often make the mistake of committing .pyc files in git repositories](https://coderwall.com/p/wrxwog/why-not-to-commit-pyc-files-into-git-and-how-to-fix-if-you-already-did)

<div class="page-break" />

#### Comparing repository clone sizes

By comparing the size and number of objects in each of the official repositories with the OpusVL amalgamated repository with we can assess the extent of the divergence from the upstream code-base.

| Repo                             | Objects | Size      |
| -------------------------------- | ------- | --------- |
| openeobs/openeobs                | 33344   | 15.37 MiB |
| openeobs/nhclinical              | 12918   | 8.58 MiB  |
| openeobs/openeobs-client-modules | 1303    | 1.05 MiB  |
| Sub-total of openeobs repos      | __47565__   | __25 MiB__    |
| opusVL/Open-eObs-Modules         | __13872__   | __12.52 MiB__ |

##### Clone summary

```sh
robd@rd-nuc:~/Work/code-compare/opusvl$ git clone git@gitlab.com:openeobs/opusVL/Open-eObs-Modules.git
Cloning into 'Open-eObs-Modules'...
Receiving objects: 100% (13872/13872), 12.52 MiB | 571.00 KiB/s, done.

robd@rd-nuc:~/Work/code-compare/openeobs$ git clone git@gitlab.com:openeobs/openeobs.git
Cloning into 'openeobs'...
Receiving objects: 100% (33344/33344), 15.37 MiB | 514.00 KiB/s, done.

robd@rd-nuc:~/Work/code-compare/compare-the-code$ git clone git@gitlab.com:openeobs/nhclinical.git
Cloning into 'nhclinical'...
Receiving objects: 100% (12918/12918), 8.58 MiB | 576.00 KiB/s, done.

robd@rd-nuc:~/Work/code-compare/compare-the-code$ git clone git@gitlab.com:openeobs/openeobs-client-modules.git
Cloning into 'openeobs-client-modules'...
Receiving objects: 100% (1303/1303), 1.05 MiB | 552.00 KiB/s, done.
```
