# source code control

https://sourcecodecontrol.co/

> Source Code Control specialises in building business processes to manage risks in open source software supply chain. Whether you are a software producer distributing a software solution or an acquirer of third party supplied software how can you be confident risk is minimised in the software supply chain.
